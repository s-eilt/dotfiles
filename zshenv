[[ -z $HOSTNAME ]] && HOSTNAME=$HOST

if [[ -z $SSH_AUTH_SOCK ]]; then
    export SSH_AGENT_PID=$(ps x | grep "[s]sh-agent" | awk '{print $1}')
    export SSH_AUTH_SOCK=$(echo /tmp/ssh-*/agent.*(=UN))
    if [[ -z $SSH_AUTH_SOCK ]]; then
        unset SSH_AGENT_PID
        unset SSH_AUTH_SOCK
    fi
fi

ORACLE_JAVA_HOME=/opt/jdk
ORACLE_JRE_HOME=$JAVA_HOME/jre
if [ -d $ORACLE_JAVA_HOME ]; then
    export JAVA_HOME
    export JRE_HOME
fi

typeset -U path
path=("$HOME/.local/bin" "$HOME/bin" "$HOME/node/bin" "$HOME/pear/bin" "$HOME/pear/pear" "$HOME/.local/pear" /opt/clang/bin $JAVA_HOME/bin $JRE_HOME/bin /home/simon/.local/scala-2.11.6/bin "$path[@]")
path=( ${^path}(N-/) ) # keep only elements that exist as a directory

PHPRC="$HOME/.local/pear"
export PHPRC

WORKON_HOME="$HOME/.virtualenvs"
export WORKON_HOME
