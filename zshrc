source $HOME/.dotfiles/antigen/antigen.zsh

antigen use oh-my-zsh

antigen bundles <<EOBUNDLES
command-not-found
cp
debian
rsync
gitignore
virtualenv
git
git-extras
git-flow
pip

zsh-users/zsh-syntax-highlighting

EOBUNDLES

if (( $+commands[virtualenvwrapper.sh] )); then
    antigen bundle virtualenvwrapper
fi

antigen theme ys

antigen apply


# fix git theme slowness
function git_prompt_info() {
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return
    echo "$ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}


setopt appendhistory extendedglob hist_ignore_all_dups noautocd noclobber rc_quotes

if [[ -d ~/.zsh-funs ]] then
  [[ $fpath = *.zsh-funs* ]] || fpath=(~/.zsh-funs $fpath)
fi
autoload -U ${fpath[1]}/*(:t)
autoload -Uz compinit; compinit

autoload -U zmv zed zcalc
alias zcp='noglob zmv -C'
alias zln='noglob zmv -L'
alias zmv='noglob zmv'

alias cp='cp --reflink=auto'

alias vem='gvim --remote-silent'
alias nvims='nvim --address=/tmp/nvim'
alias nvimc='nvimclient --address=/tmp/nvim'

HISTFILE=~/.histfile

if (( $+commands[vim] )); then
    export EDITOR=vim
elif (( $+commands[vi] )); then
    export EDITOR=vi
fi
if [ ! -z $EDITOR ]; then
    export VISUAL=$EDITOR
fi

[[ -e ~/.zshrc.local ]] && source ~/.zshrc.local
