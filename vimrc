syntax on

"jump to last position in file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" sunset config {{{
let g:sunset_latitude = 56.0
let g:sunset_longitude = -3.2
let g:sunset_utc_offset = 0 "british winter time
" }}}

call pathogen#infect()

" solarized colorscheme {{{
let g:solarized_visibility="high" "highlight listchars
if !has('gui_running')
  set t_Co=16
endif
colorscheme solarized
" }}}

if has("autocmd")
  filetype plugin indent on
endif

set showcmd		" Show (partial) command in status line.
set autoread        " Automatically read when file is changed.
set autowrite		" Automatically save before commands like :next and :make
set mouse=a		" Enable mouse usage (all modes)
set mousemodel=popup_setpos

" indent {{{
setlocal tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set lbr
set autoindent
set smartindent
set wrap
" }}}

digraph 3. 8230 " …
let mapleader = ","
let maplocalleader = ";"

map <leader>w :w!<cr>

map <silent> <C-l> :nohlsearch<cr>

map <right> :bn<cr>
map <left> :bp<cr>

map <leader>spen :setlocal spell spelllang=en<cr>
map <leader>spde :setlocal spell spelllang=de<cr>

map <leader>gtd :!gtd %<C-M>:e<C-M><C-M>

" clang-format mappings {{{
map <C-K> :pyf ~/.dotfiles/vim/clang-format.py<cr>
imap <C-K> <c-o>:pyf ~/.dotfiles/vim/clang-format.py<cr>
" }}}

" fugitive git mappings {{{
nnoremap <leader>ga :Git add %:p<CR><CR>
nnoremap <leader>gs :Gstatus<CR>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gt :Gcommit -v -q %:p<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gr :Gread<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
" }}}

nnoremap <leader>ev :vsplit $MYVIMRC<cr>

inoremap jk <Esc>

" reload vimrc when edited
autocmd! bufwritepost .vimrc source ~/.vimrc

set scrolloff=7
set display=lastline
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+

set wildmenu
set ruler
set hidden "Change buffer without saving
set backspace=indent,eol,start
set whichwrap+=<,>,h,l

set ignorecase
set smartcase
set hlsearch
set incsearch

set nolazyredraw
set laststatus=2

set magic

set showmatch
set mat=2

set encoding=utf8

set nobackup

set undodir=~/.vim_runtime/undodir
set undofile

let g:netrw_home = expand('~/.vim_runtime')

" project-specific config {{{
" {{{ widelands files
function! SE_widelands()
    if &filetype == 'cpp'
        setlocal tabstop=3
        setlocal shiftwidth=3
        setlocal softtabstop=3
        setlocal textwidth=100
        setlocal noexpandtab
        setlocal cindent
        setlocal formatoptions=tcqlron
        setlocal cinoptions=:0,l1,t0,g0,(0
        syn keyword cOperator likely unlikely
        syn keyword cType u8 u16 u32 u64 s8 s16 s32 s64
        highlight default link WlError ErrorMsg
        syn match WlError / \+\ze\t/     " spaces before tab
        syn match WlError /\s\+$/        " trailing whitespaces
        syn match WlError /\%110v.\+/     " virtual column 81 and more
    endif
endfunction
" }}}

" {{{ kernel files
function! SE_kernel()
    if &filetype == 'c'
        setlocal wildignore+=*.ko,*.mod.c,*.order,modules.builtin
        setlocal tabstop=8
        setlocal shiftwidth=8
        setlocal softtabstop=8
        setlocal textwidth=80
        setlocal noexpandtab
        setlocal cindent
        setlocal formatoptions=tcqlron
        setlocal cinoptions=:0,l1,t0,g0,(0
        syn keyword cOperator likely unlikely
        syn keyword cType u8 u16 u32 u64 s8 s16 s32 s64
        highlight default link LinuxError ErrorMsg
        syn match LinuxError / \+\ze\t/     " spaces before tab
        syn match LinuxError /\s\+$/        " trailing whitespaces
        syn match LinuxError /\%81v.\+/     " virtual column 81 and more
    endif
endfunction
" }}}

" {{{ weine-feinkost codebase
function! SE_weine()
    if &filetype == 'php'
        setlocal noet ci sts=0 sw=4 ts=4
        let b:phpqa_codesniffer_autorun = 0
        let g:phpqa_codesniffer_autorun = 0
    endif
endfunction
" }}}

function! SetupEnvironment()
    let l:path = expand('%:p')
    if l:path =~ '/home/simon/projects/shopware'
        call SE_weine()
    elseif l:path =~ '/home/simon/projects/weine'
        call SE_weine()
    elseif l:path =~ 'workspace/eudyptula'
        call SE_kernel()
    elseif l:path =~ 'widelands'
        call SE_widelands()
    endif
endfunction

augroup all_the_files
    autocmd!
    autocmd! BufReadPost,BufNewFile * call SetupEnvironment()
augroup END
" }}}

runtime! macros/matchit.vim

set fileformats+=mac

let g:javascript_conceal = 1

" python settings {{{
let python_highlight_all = 1
augroup filetype_python
    autocmd!
    autocmd FileType python syn keyword pythonDecorator True None False self
    autocmd FileType python setlocal foldmethod=indent
    autocmd FileType python setlocal foldnestmax=2 "Do not indent inside methods
augroup END
" }}}

" php settings {{{
function! MySetupPhpEnv()
    setlocal tags+=/home/simon/.local/share/ctags/php.tags
    setlocal foldmethod=indent
    if !exists("b:phpqa_codesniffer_noautorun")
        let g:phpqa_codesniffer_autorun = 1
    endif
endfunction

let g:phpqa_messdetector_ruleset = 'cleancode,codesize,design,naming,unusedcode'
let g:phpqa_codesniffer_args = "--standard=Zend"
augroup filetype_php
    autocmd!
    autocmd FileType php call MySetupPhpEnv()
augroup END
" }}}

augroup filetype_makefile
    autocmd!
    autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
augroup END

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

augroup filetype_c
    autocmd!
    autocmd FileType c setlocal foldmethod=indent
augroup END

" Add xptemplate global personal directory value
if has("unix")
  set runtimepath+=~/.dotfiles/vim/xpt-personal
endif
